<?php

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data_alter().
 */
function datetime_range_filter_between_field_views_data_alter(array &$data, FieldStorageConfigInterface $field_storage) {
  if ($field_storage->getType() === 'daterange') {
    foreach ($data as $table_name => $table_data) {
      $field_storage_name = $field_storage->getName();
      $data[$table_name][$field_storage_name . '_between'] = [
        'group' => t('Date filter between'),
        'title' => "{$field_storage_name} between filter",
        'help' => '',
        'table' => $table_name,
        'filter' => [
          'id' => 'datetime_range_filter_between',
          'entity_type' => $field_storage->getTargetEntityTypeId(),
          'field_name' => $field_storage_name,
        ],
      ];
    }
  }
}
