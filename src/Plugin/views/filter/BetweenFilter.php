<?php

namespace Drupal\datetime_range_filter_between\Plugin\views\filter;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\Sql\TableMappingInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\views\FieldAPIHandlerTrait;
use Drupal\views\Plugin\views\filter\Date;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @ViewsFilter("datetime_range_filter_between")
 */
class BetweenFilter extends Date {

  use FieldAPIHandlerTrait;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface 
   */
  protected $dateFormatter;

  /**
   * @var |\Symfony\Component\HttpFoundation\RequestStack 
   */
  protected $requestStack;

  /**
   * @var \Drupal\views\Plugin\views\query\Sql
   */
  public $query;

  /**
   * Date format for SQL conversion.
   *
   * @var string
   *
   * @see \Drupal\views\Plugin\views\query\Sql::getDateFormat()
   */
  protected $dateFormat = DateTimeItemInterface::DATE_STORAGE_FORMAT;

  /**
   * @var bool 
   */
  protected $calculateOffset;

  /**
   * {@inheritdoc}
   */
  public function operatorOptions($which = 'title') {
    return [
      'between' => $this->t('Between'),
      'not_between' => $this->t('Not between'),
    ];
  }

    /**
   * Constructs a new Date handler.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to determine the current time.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DateFormatterInterface $date_formatter, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dateFormatter = $date_formatter;
    $this->requestStack = $request_stack;

    $definition = $this->getFieldStorageDefinition();
    if ($definition->getSetting('datetime_type') === DateTimeItem::DATETIME_TYPE_DATE) {
      // Date format depends on field storage format.
      $this->dateFormat = DateTimeItemInterface::DATE_STORAGE_FORMAT;
      // Timezone offset calculation is not applicable to dates that are stored
      // as date-only.
      $this->calculateOffset = FALSE;
    }
  }

  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('date.formatter'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['operator']['default'] = 'between';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return t('Between filter');
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $exposed = $form_state->get('exposed');
    $form['value'] = [
      '#type' => 'textfield',
      '#title' => !$exposed ? $this->t('Date') : '',
      '#size' => 30,
      '#default_value' => $this->value,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getValue() {
    if (is_array($this->value)) {
      return $this->value[0];
    }
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function acceptExposedInput($input) {
    if (empty($this->options['exposed'])) {
      return TRUE;
    }

    $value = $input[$this->options['expose']['identifier']];
    if (isset($value)) {
      $this->value = $value;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    $entity_field_manager = \Drupal::service('entity_field.manager');
    assert($entity_field_manager instanceof EntityFieldManagerInterface);

    $field_storages = $entity_field_manager->getFieldStorageDefinitions($this->definition['entity_type']);
    $field_storage = $field_storages[$this->definition['field_name']];
    assert($field_storage instanceof FieldStorageDefinitionInterface);

    $table_mapping  = \Drupal::entityTypeManager()->getStorage($this->definition['entity_type'])->getTableMapping();
    assert($table_mapping instanceof TableMappingInterface);

    $start_field = $this->query->getDateFormat($this->tableAlias . '.' . $table_mapping->getFieldColumnName($field_storage, 'value'), DateTimeItemInterface::DATE_STORAGE_FORMAT);
    $end_field = $this->query->getDateFormat($this->tableAlias . '.' . $table_mapping->getFieldColumnName($field_storage, 'end_value'), DateTimeItemInterface::DATE_STORAGE_FORMAT);

    $timezone = $this->getTimezone();

    // Convert to ISO format and format for query. UTC timezone is used since
    // dates are stored in UTC.
    $value_datetime = new DateTimePlus($this->getValue(), new \DateTimeZone($timezone));
    $value_datetime = $this->query->getDateFormat($this->query->getDateField("'" . $this->dateFormatter->format($value_datetime->getTimestamp(), 'custom', DateTimeItemInterface::DATE_STORAGE_FORMAT, DateTimeItemInterface::STORAGE_TIMEZONE) . "'", TRUE, $this->calculateOffset), $this->dateFormat, TRUE);

    if ($this->operator === 'between') {
      $this->query->addWhereExpression($this->options['group'], "$start_field <= $value_datetime AND ($end_field IS NOT NULL AND $end_field >= $value_datetime)");
    }
    // Else not between
    else {
      $this->query->add_where_expression($this->options['group'], "$start_field > $value_datetime OR ($end_field IS NOT NULL AND $end_field < $value_datetime)");
    }
  }

  
  /**
   * Get the proper time zone to use in computations.
   *
   * Date-only fields do not have a time zone associated with them, so the
   * filter input needs to use UTC for reference. Otherwise, use the time zone
   * for the current user.
   *
   * @return string
   *   The time zone name.
   */
  protected function getTimezone() {
    return $this->dateFormat === DateTimeItemInterface::DATE_STORAGE_FORMAT
      ? DateTimeItemInterface::STORAGE_TIMEZONE
      : drupal_get_user_timezone();
  }

  /**
   * Get the proper offset from UTC to use in computations.
   *
   * @param string $time
   *   A date/time string compatible with \DateTime. It is used as the
   *   reference for computing the offset, which can vary based on the time
   *   zone rules.
   * @param string $timezone
   *   The time zone that $time is in.
   *
   * @return int
   *   The computed offset in seconds.
   */
  protected function getOffset($time, $timezone) {
    // Date-only fields do not have a time zone or offset from UTC associated
    // with them. For relative (i.e. 'offset') comparisons, we need to compute
    // the user's offset from UTC for use in the query.
    $origin_offset = 0;
    if ($this->dateFormat === DateTimeItemInterface::DATE_STORAGE_FORMAT && $this->value['type'] === 'offset') {
      $origin_offset = $origin_offset + timezone_offset_get(new \DateTimeZone(drupal_get_user_timezone()), new \DateTime($time, new \DateTimeZone($timezone)));
    }

    return $origin_offset;
  }

}
